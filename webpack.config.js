/**
 * ---- Импорт плагинов и модулей
 */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const { VueLoaderPlugin } = require('vue-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin')




/**
 * modifyVars for Less
 */
let styleLoaders = [{ loader: 'style-loader' }, { loader: 'css-loader' },
{
   loader: 'less-loader',
   options: {
      javascriptEnabled: true,
      modifyVars: require('./global.less.js') //require(path.resolve(__dirname,'global.less.js'))//lessVars//{'@UiBoxMenuBgColor': '#ffffff'}//lessVars//require(path.resolve(__dirname,'global.less.js')) // файл с глобальными переменными                    
   }
}];





/* ---- Режимы сборки (development/production) 
 * development - максимальная скорость загрузки, низкая производительность приложения
 * production - низкая скорость загрузки, максимальная производительность приложения
*/

const developmentMode = process.env.NODE_ENV === 'development';
const productionMode = !developmentMode

if (productionMode) {
   module.exports.devtool = 'source-map'; // при сборке бандла создается map скрипт который нужен чтобы в отладке каждый модуль выглядел как отдельный файл. Иначе сложно дебажить     
   module.exports.optimization = {  // минимизация скриптов
      minimize: true
   };
}






/* ---- Среда компляции 
 * Поскольку JavaScript может быть написан как для сервера, так и для браузера, webpack по-разному компилирует проект
 * По умолчанию используется "web"
 * Но если используется конкретный список браузерво в browserslist, то можно указать "browserslist"
*/

let target = 'web'
if (productionMode) target = 'browserslist'





/* ---- Plugins */
const plugins = [ 
   new HtmlWebpackPlugin({
      template: './src/index.html', // Данный html будет использован как шаблон
   }),
   new MiniCssExtractPlugin({
      filename: developmentMode ? './css/style.css' : './css/style.[contenthash].css', // Формат имени файла
   }),
   new VueLoaderPlugin(),
   new CopyWebpackPlugin({
      patterns: [
         { from: 'src/assets', to: 'assets'}
      ]
   })
]







/* ---- Основные настройки */
module.exports = {
   mode: 'development',
   target,
   plugins,
   entry: './src/index.ts', // точка входа 
   output: { // выходная точка
      publicPath: '/',
      path: path.resolve(__dirname, 'dist'),
      filename: developmentMode? './js/bundle.js' : './js/bundle.[contenthash].js',
      clean: true, // Очищает директорию dist перед обновлением бандла
   },
   

   devtool: 'source-map', // при сборке бандла создается map скрипт который нужен чтобы в отладке каждый модуль выглядел как отдельный файл. Иначе сложно дебажить 

   devServer: {
      client: {
         overlay: { // отображает в браузере overlay с ошибками (предупреждения отключены)
            errors: true,
            warnings: false,
         },
         progress: true, // показывает прогресс загрузки webpack
      },
      port: 9095,
      compress: true, // сжатие файлов
      historyApiFallback: true,
      magicHtml: true,
      static: {
         directory: path.join(__dirname, 'assets') // Сообщите серверу, откуда передавать контент. Это необходимо только в том случае, если вы хотите обслуживать статические файлы. static.publicPathбудет использоваться для определения того, откуда следует обслуживать пакеты, и имеет приоритет.
         ['assets'] // Этот параметр позволяет настроить параметры для обслуживания статических файлов из каталога
      },
      watchFiles: ['src/**/*']
   },

   /* Оптимизация (сжатие) css */
   optimization: {
      minimizer: [
         "...",
         new CssMinimizerPlugin()
      ],
   },

   resolve: {
      extensions: ['.ts', '.js', '.json', '.vue'],
      alias: {
         vue$: 'vue/dist/vue.js',
         "@": path.resolve(__dirname, "./src"),
         "@pages": path.resolve(__dirname, "./src/pages"),
         "@layouts": path.resolve(__dirname, "./src/layouts"),
         "@components": path.resolve(__dirname, "./src/components"),
         "@controls": path.resolve(__dirname, "./src/controls"),
         "@models": path.resolve(__dirname, "./src/models"),
      },
   },


   /**
    * Настройка Louders (загрузчиков)
    */
   module: {
      rules: [
         /**
          * Images
          * Для этого изображения необходимо импортировать в js/css
          */
         {
            test: /\.(png|jpe?g|gif|svg|webp)(\?\S*)?$/i,
            loader: 'img-optimize-loader',
            options: {
               name: './img/[name].[ext]',
               compress: {
                  mode: 'high',
                  // loseless compression for png
                  optipng: { 
                     optimizationLevel: 4,
                  },
                  // lossy compression for png. This will generate smaller file than optipng.
                  pngquant: {
                     quality: [0.3, 0.5],
                  },
                  // Compression for svg.
                  svgo: true,
                  // Compression for gif.
                  gifsicle: {
                     optimizationLevel: 3,
                  },
                  // Compression for jpg.
                  mozjpeg: {
                     progressive: true,
                     quality: 100,
                  },
                  // Compression for webp.
                  // You can also tranform jpg/png into webp.
                  webp: {
                     quality: 100,
                  },
               },
            },
         },

         /**
          * Шрифты
          */
         {
            test: /\.(eot|svg|ttf|woff|woff2|ttc|otf)(\?\S*)?$/i,
            type: 'asset/resource',
         },

         /**
          * Babel (преобразование js-код ES6+ в старый синтаксис для поддержки старых версий браузеров)
          */
         {
            test: /\.js$/,
            exclude: /node_modules/, // не обрабатываем файлы из node_modules
            use: {
               loader: 'babel-loader',
               options: {
                  cacheDirectory: true, // Использование кэша для избежания рекомпиляции при каждом запуске
               },
            },
         },

         /**
          * Styles (css, less, sass/scss)
          */
         {
            test: /\.(css|s[ac]ss)$/i,
            use: [MiniCssExtractPlugin.loader, 'css-loader', { loader: 'postcss-loader' }, 'sass-loader'] // Использование loaders происходит справа налево
         },
         {
            test: /\.less$/i,
            use: [MiniCssExtractPlugin.loader, 'css-loader', { loader: 'postcss-loader' }, 'less-loader'], // Использование loaders происходит справа налево
         },

         /**
          * TypeScript
          */
         {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            options: {
               transpileOnly: true,  // для ускорения сборки проекта
               experimentalWatchApi: true,  // для ускорения сборки проекта
               appendTsSuffixTo: [/\.vue$/]
            }
         },

         /**
          * Vue
          */
         {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
               loaders: {
                  ts: 'ts-loader',
                  options: {
                     transpileOnly: true,// для ускорения сборки проекта
                     experimentalWatchApi: true, //для ускорения сборки проекта
                  },
                  /**
                   * Если используется less, можно подключить файл с переменными для global.less.js
                   * Также, если используется Less, раскоментировать код ниже
                   */
                  // less: styleLoaders
                  // less: [ 
                  //     'css-loader',
                  //     {
                  //         loader: 'less-loader',
                  //         options: {
                  //             modifyVars: require(path.resolve(__dirname, 'global.less.js')) // файл с глобальными переменными                    
                  //         }
                  //     }
                  // ]

                  scss: [MiniCssExtractPlugin.loader, 'css-loader', { loader: 'postcss-loader' }, 'sass-loader'] // <style lang="scss">
               },
            }
         }
      ]
   }
}






/**
 * ---------------------
 * ОБРАТИ ВНИМАНИЕ!!!
 * ---------------------
 */

/**
 * 1. sass-loader не работает с версией 8.0.0. и vue cli 3.10.0 Данная проблема связана с плагином "mini-css-extract-plugin".
 *    Он перестал поддерживать в последних версиях "из коробки" sass-loader с синтаксисом "sass" и поддерживает только "scss"
 *    Как уже всем известно scss новая версия sass так что придется новую версию sass'a использовать, хотя кое-что попробовать все-таки стоит:
 *    https://qna.habr.com/q/747197, но это уже костыли
 *    Поэтому в данной сборке предпочтительнее использовать SCSS, CSS или Less
 */


/**
 * 2. Плагин "mini-css-extract-plugin" уже извлекает код в теге <style> из компонентов .vue и применяет к ним вендорные префиксы css,
 *    а также добавляет эти стили в скомпилированный файл .сss в итоговой сборке production при выполнее npm run build
 *    и для этого даже не нужно использовать "vue-style-loader"
 */


/**
 * 3. Данная сборка нацелена на то, чтобы совместить последнюю версию webpack 5 (5.65.0) и Vue 2 (2.6.14)
 *    И при стандартной установке модулей Vue (без указания версий) может возникать ошибка:
 *    Error : vue-loader requires @vue/compiler-sfc to be present in the dependency tree
 *    происходит это из-за того, что будет использоваться vue-loader версии (16) для Vue 3 в проекте Vue 2, vue-loader 16-ой версии конфликтует с vue 2,
 *    чтобы этого избежать, устанавливаем vue-loader 15-ой версии, можно выбрать 15.9.7
 *    npm install --save-dev vue-loader@15.9.7
 */


/**
 * 4. Если используется версия node js ниже 12.20.0 возникнет ошибка с использованием "copy-webpack-plugin"  версии 10 и выше
 *    HookWebpackError: Not supported (http://prntscr.com/26d4zla).
 *    Есть 2 варианта:
 *       1. Обновить node js до версии 12.20.0 или выше   -   npm install node@12.20.0 -g
 *       2. Установить "copy-webpack-plugin" версии 9   -   npm install copy-webpack-plugin@9 --save-dev
 */





/**
 * ---------------------
 * ПОЛЕЗНЫЕ ССЫЛКИ!!!
 * ---------------------
 */

/**
 * browserslist 
 * https://github.com/browserslist/browserslist
 */

/**
 * tsconfig.json
 * https://www.typescriptlang.org/tsconfig
 */

