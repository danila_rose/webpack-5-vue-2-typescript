/**
 * ГЛАВНЫЙ ФАЙЛ (entry point)
 * Здесь подключаются модули, плагины, фильтры, vue и т.д 
 */
import Vue from "vue"
import router from './router'


/**
 * Подключение стилей, изображений для копирования в dist
 */
import "./scss/style.scss"

/**
 * Подключение внутренних файлов Vue ( подключение директив, регистрации компонентов и т.д.)
 */
import './register-router-hooks'
import './register-components'
import './register-directives'
import app from "./app.vue"


/**
 * Импорт плагинов
 */
import mainStore from './plugins/mainStore'
Vue.use(mainStore);

import domPlugin from './plugins/DomPlugin'
Vue.use(domPlugin);

const sayHello = () => { console.log("Hello, World!"); }
sayHello();




/**
 * Объявление плагинов
 */
const moment = require('moment')




/**
 * Подключение плагинов
 */
moment.locale('ru');
Vue.use(require('vue-moment'), {
   moment
});








/**
 * Экземпляр главного компонента Vue
 */
const v = new Vue({
   el: "#app",
   router: router,
   // store: store,
   template: `<app/>`,
   components: {
      app,
      // uiComponents  
   }
});